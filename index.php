<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_stratup_errors', 1);

require_once 'app/header.php';

$posts = get_recent_posts(3);

?>

<main>
    <div class="wrapper">
        <h1>Recent posts:</h1>
        <div class="recent-posts">
            <?php foreach ($posts as $post): ?>
                <div class="post">
                    <!-- ?post_id is a GET parameter -->
                    <a href="/post.php?post_id=<?= $post['id'] ?>" class="post__image">
                        <img src="<?= $post['image'] ?>" alt="post-image">
                    </a>
                    <div class="post__text-container">
                        <a href="/post.php?post_id=<?= $post['id'] ?>" class="post-title">
                            <h2><?= $post['title'] ?></h2>
                        </a>
                        <?php if($post['text'] && strlen($post['text']) > 256): ?>
                            <p><?= mb_substr($post['text'], 0, 256, 'UTF-8').'...' ?></p>
                        <?php else: ?>
                            <p><?= $post['text'] ?></p>
                        <?php endif; ?>
                        <a href="/post.php?post_id=<?= $post['id'] ?>" class="button">Read more</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</main>

<?php include_once 'app/footer.php' ?>
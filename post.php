<?php 
 
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_stratup_errors', 1);

require_once 'app/header.php';

$post_id = $_GET['post_id'] ?? 1;

if (!is_numeric($post_id)) {
    redirect_to_404();
}

$post = get_posts_by_id($post_id);

if (!$post) {
    redirect_to_404();
}

?>

<main class="post-page">
    <div class="wrapper">
        <div class="post-page__image">
            <img src="<?= $post['image'] ?>" alt="post-image">
        </div>
        <h1 class="post-page__title"><?= $post['title'] ?></h1>
        <p class="post-page__content"><?= $post['text'] ?></p>
        <a href="/blog.php" class="button">Back to blog</a>
    </div>
</main>

<?php include_once 'app/footer.php' ?>
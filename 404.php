<?php

require_once 'app/header.php';

?>

<main class="error-page">
    <div class="wrapper">
        <h1>404</h1>
        <a href="/" class="button">Back to startpage</a>
    </div>
</main>

<?php include_once 'app/footer.php' ?>
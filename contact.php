<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_stratup_errors', 1);

require_once 'app/header.php';

if (isset($_POST['email']) && isset($_POST['text'])) {
    $email = trim($_POST['email']);
    $text = trim($_POST['text']);

    $result_message = insert_message($email, $text);
    header('/contact.php');
}

?>

<main>
    <div class="wrapper">
        <h1>Contact us</h1>
        <form action="/contact.php" method="POST">
            <input type="email" name="email" value="" placeholder="Enter your email" required>
            <textarea name="text" placeholder="Enter your message" cols="30" rows="10" required></textarea>
            <button type="submit">Send</button>
        </form>

        <p class="result-message<?= $result_message == 'Form sent' ? ' success' : ' fail' ?>">
            <?= $result_message ?? '' ?>
        </p>
    </div>
</main>

<?php include_once 'app/footer.php' ?>
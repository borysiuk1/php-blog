<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_stratup_errors', 1);

require_once 'app/header.php';

$blog_posts = blog_posts(3);

$page = $blog_posts[0];
$total_pages = $blog_posts[1];
$posts = $blog_posts[2];

?>

<main>
    <div class="wrapper">
        <h1>Blog</h1>

        <div class="posts-list">
            <?php foreach ($posts as $post): ?>
                <div class="post">
                    <a href="/post.php?post_id=<?= $post['id'] ?>" class="post__image">
                        <img src="<?= $post['image'] ?>" alt="post-image">
                    </a>
                    <div class="post__text-container">
                        <a href="/post.php?post_id=<?= $post['id'] ?>" class="post-title">
                            <h2><?= $post['title'] ?></h2>
                        </a>
                        <?php if($post['text'] && strlen($post['text']) > 256): ?>
                            <p><?= mb_substr($post['text'], 0, 256, 'UTF-8').'...' ?></p>
                        <?php else: ?>
                            <p><?= $post['text'] ?></p>
                        <?php endif; ?>
                        <a href="/post.php?post_id=<?= $post['id'] ?>" class="button">Read more</a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <ul class="pagination">
            <li class="<?= $page <= 1 ? 'disabled' : '' ?>">
                <a href="<?= $page <= 1 ? '#' : '?page='.($page - 1) ?>">Prev</a>
            </li>
            <li class="<?= $page >= $total_pages ? 'disabled' : '' ?>">
                <a href="<?= $page >= $total_pages ? '#' : '?page='.($page + 1) ?>">Next</a>
            </li>
        </ul>

    </div>
</main>

<?php include_once 'app/footer.php' ?>
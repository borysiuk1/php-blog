<?php

function get_recent_posts($posts_quantity) {
    global $link;

    $sql = "SELECT * FROM posts ORDER BY id DESC LIMIT $posts_quantity";

    $result = mysqli_query($link, $sql);

    $posts = mysqli_fetch_all($result, MYSQLI_ASSOC);

    return $posts;
}

function get_posts_by_id($post_id) {
    global $link;

    $sql = "SELECT * FROM posts WHERE id = $post_id";

    $result = mysqli_query($link, $sql);

    $post = mysqli_fetch_assoc($result);

    return $post;
}

function blog_posts($posts_quantity_per_page) {
    global $link;

    if (isset($_GET['page'])) {
        $page = $_GET['page'];
    } else {
        $page = 1;
    }

    $per_page = $posts_quantity_per_page;
    $offset = ($page - 1) * $per_page;

    $total_pages_sql = "SELECT COUNT(*) FROM posts";
    $total_pages_result = mysqli_query($link, $total_pages_sql);
    $total_rows = mysqli_fetch_array($total_pages_result)[0];
    $total_pages = ceil($total_rows / $per_page);

    $posts_sql = "SELECT * FROM posts ORDER BY id DESC LIMIT $offset, $per_page";
    $posts_result = mysqli_query($link, $posts_sql);
    $posts = mysqli_fetch_all($posts_result, MYSQLI_ASSOC);

    return [$page, $total_pages, $posts];
}

function insert_message($email, $text) {
    global $link;

    //prevent sql injections
    $email = mysqli_real_escape_string($link, $email);
    $text = mysqli_real_escape_string($link, $text);

    $insert_query = "INSERT INTO messages (email, text) VALUES ('$email', '$text')";

    $result = mysqli_query($link, $insert_query);

    if ($result) {
        return 'Form sent';
    } else {
        return 'Fail';
    }
}

function redirect_to_404() {
    //Send 404 response to client
    http_response_code(404);
    //Include custom 404 page
    include './404.php';
    //Kill the script
    exit;
}

?>
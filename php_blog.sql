-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: database
-- Generation Time: Jan 17, 2022 at 12:01 PM
-- Server version: 5.7.29
-- PHP Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `email`, `text`) VALUES
(1, 'vfd@gmail.com', 'ppppp'),
(2, 'borysyuk.sasha@gmail.com', '1111'),
(3, 'cd@gmail.com', '22222'),
(4, 'borysyuk.sasha@gmail.com', 'sfsdfds'),
(5, 'borysyuk.sasha@gmail.com', 'sfsdfds'),
(6, 'cd@gmail.com', '00000'),
(7, 'cd@gmail.com', '00000'),
(8, 'cd@gmail.com', '00000'),
(9, 'test@gmail.com', 'Test message'),
(10, 'test@gmail.com', 'Test message'),
(11, 'aaa@gmail.com', 'aaa'),
(12, 'aaa@gmail.com', 'aaa'),
(13, 'aaa@gmail.com', 'aaa'),
(14, 'aaa@gmail.com', 'aaa'),
(15, 'a@gmail.com', 'zzzzz'),
(16, 'borysyuk.sasha@gmail.com', 'wrwerwe'),
(17, 'borysyuk.sasha@gmail.com', 'wrwerwe'),
(18, 'testalkima@gmail.com', 'Test alkima'),
(19, 'testalkima@gmail.com', 'Test alkima'),
(20, 'cd@gmail.com', 'dfdf'),
(21, 'cd@gmail.com', 'wfefef frefer'),
(22, 'testalkima@gmail.com', '1112222'),
(23, 'testalkima@gmail.com', '1112222'),
(24, 'borysyuk.sasha@gmail.com', '1111');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `text` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `text`, `image`) VALUES
(1, 'Post 1', 'Text 1', 'https://cdn3.dpmag.com/2021/07/Landscape-Tips-Mike-Mezeul-II.jpg'),
(2, 'Post 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae nibh eu nunc facilisis auctor at vitae leo. Nam vestibulum ligula sit amet risus vehicula volutpat. Ut semper convallis velit ac convallis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec vel vulputate purus. Proin sem dolor, dictum in semper sit amet, vehicula id neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Maecenas vel tempus erat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum commodo, leo sit amet feugiat sodales, dui nisl iaculis ante, in aliquet dolor magna sit amet urna. Sed pellentesque molestie libero non porta. Nullam eu felis ut erat porta congue ac eu ante. Nam gravida elit ante, vel mattis libero semper vitae. Vivamus sit amet bibendum elit.', 'https://cdn3.dpmag.com/2021/07/Landscape-Tips-1-Mike-Mezeul-II.jpg'),
(3, 'Post 3', 'Lorem ipsum dolor.', 'https://cdn3.dpmag.com/2021/07/Landscape-Tips-3-Mike-Mezeul-II.jpg'),
(4, 'Post 4', 'Some text.', 'https://cdn3.dpmag.com/2021/07/Landscape-Tips-4-Mike-Mezeul-II.jpg'),
(5, 'Post 5', 'Content about post 5.', 'https://cdn3.dpmag.com/2021/07/Landscape-Tips-5-Mike-Mezeul-II-1.jpg'),
(6, 'Post 6', 'Random text.', 'https://cdn3.dpmag.com/2021/07/Landscape-Tips-6-Mike-Mezeul-II.jpg'),
(7, 'Post 7', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 'https://www.superprof.com.au/blog/wp-content/uploads/2018/02/landscape-photography-tutorials.jpg'),
(8, 'Post 8', 'Post 8 text.', 'https://www.mickeyshannon.com/photos/thumbs2x/mountain-photography.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
